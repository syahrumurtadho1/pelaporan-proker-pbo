-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Jan 2020 pada 17.40
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uas_proker`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `npp_dosen` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `nama_dosen` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `no_telpon` varchar(255) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`npp_dosen`, `id_jabatan`, `nama_dosen`, `tanggal_lahir`, `tempat_lahir`, `no_telpon`, `alamat`) VALUES
(1010, 5, 'Admin Prodi', '2020-01-07', 'Indonesia', '081231241', 'Indonesia'),
(2121, 4, 'Syaiful Ardhi', '1975-01-14', 'Surabaya', '0812331412', 'Jl.Nginden Intan No.12'),
(123123, 1, 'Syahru Murtadho', '1973-12-17', 'Surabaya', '081357057001', 'Jl.Kalibokor Timur No.117');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE `fakultas` (
  `id_fakultas` int(11) NOT NULL,
  `nama_fakultas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
(1, 'Rektor'),
(2, 'Wakil Rektor'),
(3, 'Wakil Rektor II'),
(4, 'Dosen Umum'),
(5, 'Admin Prodi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_proker`
--

CREATE TABLE `laporan_proker` (
  `id_laporan` int(11) NOT NULL,
  `nama_proker` varchar(255) NOT NULL,
  `tgl_realisasi` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `uraian` text NOT NULL,
  `angg_digunakan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laporan_proker`
--

INSERT INTO `laporan_proker` (`id_laporan`, `nama_proker`, `tgl_realisasi`, `tgl_selesai`, `uraian`, `angg_digunakan`) VALUES
(1, 'Nama Program Kerja 1', '2020-02-06', '2020-03-12', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ', 2500000),
(2, 'Pembangunan Jembatan', '2020-01-15', '2020-03-12', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ', 15000000),
(3, 'Pembuatan Gorong Gorong', '2020-01-16', '2020-02-11', 'Tidak ada uraian', 2500000),
(4, 'Pengembangan Aplikasi Desa Pintar', '2020-01-10', '2020-04-07', 'Tes Combobox', 100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengurus_fakultas`
--

CREATE TABLE `pengurus_fakultas` (
  `id_pengurus` int(11) NOT NULL,
  `id_fakultas` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `npp_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE `prodi` (
  `id_prodi` int(11) NOT NULL,
  `id_fakultas` int(11) NOT NULL,
  `nama_prodi` varchar(255) NOT NULL,
  `id_kaprodi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `program_kerja`
--

CREATE TABLE `program_kerja` (
  `id_proker` int(11) NOT NULL,
  `nama_proker` varchar(255) NOT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `angg_digunakan` double NOT NULL,
  `lokasi_proker` varchar(255) NOT NULL,
  `uraian` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `program_kerja`
--

INSERT INTO `program_kerja` (`id_proker`, `nama_proker`, `tgl_pelaksanaan`, `tgl_selesai`, `angg_digunakan`, `lokasi_proker`, `uraian`, `status`) VALUES
(1, 'Pembuatan Jembatan Gantung di Malang', '2020-01-15', '2020-02-13', 5200000, 'Malang Jawa Timur', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 1),
(2, 'Pengembangan Aplikasi Desa Pintar', '2020-01-22', '2020-02-13', 1250000, 'Sidoarjo Jawa Timur', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `npp_dosen` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `npp_dosen`, `password`, `status`) VALUES
(1, 123123, '123', 1),
(2, 2121, '123', 0),
(3, 1010, '123', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`npp_dosen`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indeks untuk tabel `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indeks untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indeks untuk tabel `laporan_proker`
--
ALTER TABLE `laporan_proker`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indeks untuk tabel `pengurus_fakultas`
--
ALTER TABLE `pengurus_fakultas`
  ADD PRIMARY KEY (`id_pengurus`),
  ADD KEY `pengurus_fakultas_ibfk_1` (`id_fakultas`),
  ADD KEY `pengurus_fakultas_ibfk_2` (`npp_dosen`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indeks untuk tabel `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id_prodi`),
  ADD KEY `id_fakultas` (`id_fakultas`),
  ADD KEY `id_kaprodi` (`id_kaprodi`);

--
-- Indeks untuk tabel `program_kerja`
--
ALTER TABLE `program_kerja`
  ADD PRIMARY KEY (`id_proker`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `npp_dosen` (`npp_dosen`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id_fakultas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `laporan_proker`
--
ALTER TABLE `laporan_proker`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD CONSTRAINT `dosen_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengurus_fakultas`
--
ALTER TABLE `pengurus_fakultas`
  ADD CONSTRAINT `pengurus_fakultas_ibfk_1` FOREIGN KEY (`id_fakultas`) REFERENCES `fakultas` (`id_fakultas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengurus_fakultas_ibfk_2` FOREIGN KEY (`npp_dosen`) REFERENCES `dosen` (`npp_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengurus_fakultas_ibfk_3` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `prodi`
--
ALTER TABLE `prodi`
  ADD CONSTRAINT `prodi_ibfk_1` FOREIGN KEY (`id_kaprodi`) REFERENCES `dosen` (`npp_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodi_ibfk_2` FOREIGN KEY (`id_fakultas`) REFERENCES `fakultas` (`id_fakultas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodi_ibfk_3` FOREIGN KEY (`id_kaprodi`) REFERENCES `dosen` (`npp_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`npp_dosen`) REFERENCES `dosen` (`npp_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
