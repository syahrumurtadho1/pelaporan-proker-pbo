/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;
import Code.koneksi;
import Code.Program_kerja;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import uas_pbo.LaporanProker;
import uas_pbo.Login;

/**
 *
 * @author Hafiz
 */
public class User {
    /** @pdOid 34920b19-2ed9-42a4-9739-af44382ee8a5 */
   private int user_id;
   /** @pdOid 9093c873-658c-4862-ab86-8c268425439a */
   private int npp_dosen;
   /** @pdOid daab7c1a-3245-40c8-a128-6c8f5e5a0de0 */
   private String password;
   /** @pdOid b474b519-1a2e-4cbb-8271-3e5be66415b4 */
   private int status;
   public static int i;
   public static String j;
   
   public User() {
   }
   /** @param Status
    * @pdOid 86d065c4-67be-453e-bae2-cbbe31d27812 */

   /** @pdOid e20570fe-2995-4b32-b09f-8cb0ecd6271e */
   public int getuser_id() {
      return user_id;
   }
   
   /** @param newUser_id
    * @pdOid 56280830-ea62-4f25-9bce-71b4b391bf3d */
   public void setuser_id(int newUser_id) {
      user_id = newUser_id;
   }
   
   /** @pdOid 08888d19-9b58-4f2e-bcbe-33958e1c7f1a */
   public String getpassword() {
      return password;
   }
   
   /** @param newPassword
    * @pdOid 128525b8-b595-45f6-8868-5216bf56d5a1 */
   public void setpassword(String newPassword) {
      password = newPassword;
   }
   
   /** @pdOid 271e3efe-2d6b-4d70-b8ca-3778e5a86b11 */
   public int getstatus() {
      return status;
   }
   
   /** @param newStatus
    * @pdOid b0ec45ea-54e8-407c-ba13-d2325a68a7b8 */
   public void setstatus(int newStatus) {
      status = newStatus;
   }
   
   /** @pdOid 73ba2410-ad61-4739-8052-f8e97a7cba59 */
   public int getnpp_dosen() {
      return npp_dosen;
   }
   
   /** @param newNpp_dosen
    * @pdOid 3c544c9a-48cd-4c3e-a993-673b17f178ae */
   public void setnpp_dosen(int newNpp_dosen) {
      npp_dosen = newNpp_dosen;
   }  
   public int ambilnpp;
   public int getambilnpp(){
       return ambilnpp;
   }
   public void login() throws Exception{
       JDialog dialog = new JDialog();
        dialog.setAlwaysOnTop(true);
           int j = getnpp_dosen();
           
           String sql ="select * from user users inner join dosen dosenku on users.npp_dosen = dosenku.npp_dosen inner join jabatan jabatanku on dosenku.id_jabatan = jabatanku.id_jabatan where users.npp_dosen='"+getnpp_dosen()+"' and users.password='"+getpassword()+"'";    
           Statement st = koneksi.getConnection().createStatement();
           ResultSet rs = st.executeQuery(sql);
           
           if(rs.next()){
                    
                if(rs.getInt("status") < 1){
                   JOptionPane.showMessageDialog(null, "Akun anda sudah tidak aktif");
                }
                   
                    else {
                        if (rs.getInt("id_jabatan")==1){
                             JOptionPane.showMessageDialog(dialog, "Selamat Datang " + rs.getString("nama_dosen")+" | " + rs.getString("nama_jabatan"));
                                LaporanProker ml = new LaporanProker();
                                ml.setVisible(true);
                                ml.setLocationRelativeTo(null);
                                i=1;

                             }
                         else if (rs.getInt("id_jabatan")==2){
                            JOptionPane.showMessageDialog(dialog, "Selamat Datang " + rs.getString("nama_dosen")+" | " + rs.getString("nama_jabatan"));
                                LaporanProker ml = new LaporanProker();
                                ml.setVisible(true);
                                ml.setLocationRelativeTo(null);
                                i=1;
                             }
                         else if (rs.getInt("id_jabatan")==3){
                            JOptionPane.showMessageDialog(dialog, "Selamat Datang " + rs.getString("nama_dosen")+" | " + rs.getString("nama_jabatan"));
                                LaporanProker ml = new LaporanProker();
                                ml.setVisible(true);
                                ml.setLocationRelativeTo(null);
                                i=1;
                            }
                        else if (rs.getInt("id_jabatan")==4){
                            JOptionPane.showMessageDialog(dialog, "Maaf anda tidak bisa mengakses aplikasi ini");
//                            kaprodi kp = new kaprodi();
//                            kp.setVisible(true);
//                            kp.setLocationRelativeTo(null);
//                            i=1;
                            }
                        else if (rs.getInt("id_jabatan")==5){
                            JOptionPane.showMessageDialog(dialog, "Selamat Datang " + rs.getString("nama_dosen")+" | " + rs.getString("nama_jabatan"));
//                            kaprodi kp = new kaprodi();
//                            kp.setVisible(true);
//                            kp.setLocationRelativeTo(null);
//                            i=1;
                            }
                    }
           }
           else {
               JOptionPane.showMessageDialog(null, "NPP atau PASSWORD salah");
           }
           j = getnpp_dosen();
           setnpp_dosen(npp_dosen);
           setpassword(password);
    }
   public int keluar(){
       return i;
   }

    private void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
